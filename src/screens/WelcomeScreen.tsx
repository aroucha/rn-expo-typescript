import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import {
	convertDateToString,
	convertTimestampToString,
	convertTimestampToStringMotnhAndYear,
	convertIntDateToString,
} from '../utils/dateUtils';

export class WelcomeScreen extends React.Component {
	render() {
		const moment = new Date();
		const firstString = convertDateToString(moment);
		const secondString = convertTimestampToString(moment.getTime());
		const thirdString = convertTimestampToStringMotnhAndYear(moment.getTime());
		const fourthString = convertIntDateToString(
			moment.getDate(),
			moment.getMonth(),
			moment.getFullYear()
		);
		return (
			<View style={styles.container}>
				<Text>{`Welcome! Some info when this app started execution:`}</Text>
				<Text>{firstString}</Text>
				<Text>{secondString}</Text>
				<Text>{thirdString}</Text>
				<Text>{fourthString}</Text>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
