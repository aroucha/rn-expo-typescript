import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { TouchablePlatform } from '../shared/TouchablePlatform';
import { NavigationScreenProp, NavigationState } from 'react-navigation';

type Props = {
	navigation: NavigationScreenProp<NavigationState>;
};

export class GameSelectionScreen extends React.Component<Props> {
	_goToGameOfLife = () => {
		this.props.navigation.navigate('GameOfLife');
	};
	render() {
		return (
			<View style={styles.container}>
				<Text>{`GameSelectionScreen`}</Text>
				<TouchablePlatform onPress={this._goToGameOfLife}>
					<Text>{`Go to Game of Life`}</Text>
					<Text>{`NOW`}</Text>
				</TouchablePlatform>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center',
	},
});
