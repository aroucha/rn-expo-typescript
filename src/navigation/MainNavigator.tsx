import { createBottomTabNavigator, createStackNavigator } from 'react-navigation';
import { WelcomeScreen } from '../screens/WelcomeScreen';
import { GameSelectionScreen } from '../screens/GameSelectionScreen';
import { GameOfLifeScreen } from '../screens/GameOfLifeScreen';

const HomeTabNavigator = createBottomTabNavigator({
	Home: {
		screen: WelcomeScreen,
	},
	GameSelection: {
		screen: GameSelectionScreen,
	},
});

export default createStackNavigator({
	Home: {
		screen: HomeTabNavigator,
	},
	GameOfLife: {
		screen: GameOfLifeScreen,
	},
});
