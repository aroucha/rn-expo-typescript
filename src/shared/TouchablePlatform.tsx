import * as React from 'react';
import { TouchableOpacity, View } from 'react-native';

type Props = {
	containerStyle?: any;
	onPress: () => void;
};

export class TouchablePlatform extends React.Component<Props> {
	render() {
		const { children, containerStyle, onPress } = this.props;
		return (
			<TouchableOpacity onPress={onPress}>
				<View style={containerStyle}>{children}</View>
			</TouchableOpacity>
		);
	}
}
