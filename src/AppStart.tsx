import * as React from 'react';
import { View, Text, StyleSheet } from 'react-native';
import MainNavigator from './navigation/MainNavigator';

export class AppStart extends React.Component {
	render() {
		return (
			<View style={styles.appContainer}>
				<MainNavigator />
			</View>
		);
	}
}

const styles = StyleSheet.create({
	appContainer: {
		flex: 1,
	},
});
