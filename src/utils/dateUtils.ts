const convertWeekDayToString = (weekDay: number): string => {
	if (weekDay === 0) {
		return 'Sunday';
	} else if (weekDay === 1) {
		return 'Monday';
	} else if (weekDay === 2) {
		return 'Tuesday';
	} else if (weekDay === 3) {
		return 'Wednesday';
	} else if (weekDay === 4) {
		return 'Thusrday';
	} else if (weekDay === 5) {
		return 'Friday';
	} else if (weekDay === 6) {
		return 'Saturday';
	}
	return '';
};
const convertMonthIntToString = (monthInt: number): string => {
	if (monthInt === 0) {
		return 'January';
	} else if (monthInt === 1) {
		return 'February';
	} else if (monthInt === 2) {
		return 'March';
	} else if (monthInt === 3) {
		return 'April';
	} else if (monthInt === 4) {
		return 'May';
	} else if (monthInt === 5) {
		return 'June';
	} else if (monthInt === 6) {
		return 'July';
	} else if (monthInt === 7) {
		return 'August';
	} else if (monthInt === 8) {
		return 'September';
	} else if (monthInt === 9) {
		return 'October';
	} else if (monthInt === 10) {
		return 'November';
	} else if (monthInt === 11) {
		return 'December';
	}
	return '';
};
const convertMonthIntToTinyString = (monthInt: number): string => {
	if (monthInt === 0) {
		return 'Jan';
	} else if (monthInt === 1) {
		return 'Feb';
	} else if (monthInt === 2) {
		return 'Mar';
	} else if (monthInt === 3) {
		return 'Apr';
	} else if (monthInt === 4) {
		return 'May';
	} else if (monthInt === 5) {
		return 'Jun';
	} else if (monthInt === 6) {
		return 'Jul';
	} else if (monthInt === 7) {
		return 'Aug';
	} else if (monthInt === 8) {
		return 'Sep';
	} else if (monthInt === 9) {
		return 'Oct';
	} else if (monthInt === 10) {
		return 'Nov';
	} else if (monthInt === 11) {
		return 'Dec';
	}
	return '';
};
export const convertDateToString = (date: Date): string => {
	const year = date.getFullYear();
	const monthInt = date.getMonth();
	const month = convertMonthIntToString(monthInt);
	const day = date.getDate();
	return `${month} ${day} ${year}`;
};
export const convertIntDateToString = (day: number, month: number, year: number): string => {
	const date = new Date(year, month, day);
	const weekDay = convertWeekDayToString(date.getDay());
	const monthString = convertMonthIntToString(month);
	const daySuffix =
		day === 1 || day === 21 || day === 31
			? 'st'
			: day === 2 || day === 22
				? 'nd'
				: day === 3 || day === 23
					? 'rd'
					: 'th';
	return `${weekDay} ${monthString} ${day}${daySuffix}`;
};
export const convertIntHoursDateToTimestamp = (
	hours: number,
	day: number,
	month: number,
	year: number
): number => {
	const date = new Date(year, month, day, hours);
	return date.getTime();
};
export const convertTimestampToString = (timestamp: number): string => {
	const date = new Date(timestamp);
	const hours = date.getHours();
	const minutes = date.getMinutes();
	const month = convertMonthIntToTinyString(date.getMonth());
	const day = date.getDate();
	return `${hours}:${minutes < 10 ? '0' + minutes : minutes} on ${month} ${day}`;
};
export const convertTimestampToStringMotnhAndYear = (timestamp: number): string => {
	const date = new Date(timestamp);
	const month = convertMonthIntToTinyString(date.getMonth());
	const year = date.getFullYear();
	return `${month} ${year}`;
};
