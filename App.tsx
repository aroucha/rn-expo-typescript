import * as React from 'react';
import { AppStart } from './src/AppStart';

export default class App extends React.Component {
	render() {
		return <AppStart />;
	}
}
