#!/bin/bash
echo "deleting node_modules"
rm -rf ./node_modules
echo "deleting yarn.lock"
rm ./yarn.lock

echo "installing node_modules"
yarn
