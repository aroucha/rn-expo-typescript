# React Native - Expo - Typescript
Project as experiment of react native with typescript.

Based on [boilerplate](https://github.com/rauldeheer/expo-typescript) by [rauldeheer](https://github.com/rauldeheer/)

## Future works

- React Navigation
- Redux
- Localization
- Tests
- Assets


## Recommended VSCode Extensions

- Prettier - Code formatter
- ESLint
- React Native Tools (?)